//  Created by denis svinarchuk on 03.11.15.
//  Copyright © 2015 Degradr.Photo. All rights reserved.
//  BSD LICENSED to Alex Sedykh 07.09.2021

#import <Foundation/Foundation.h>
#include <Accelerate/Accelerate.h>
#include <GLKit/GLKMath.h>
#import "One.h"

typedef enum {
    DP_CLUT_TYPE_1D,
    DP_CUBE_LUT_TYPE_3D
}DPCLUTType;

typedef GLKVector2 DPVector2;
typedef GLKVector3 DPVector3;
typedef GLKVector4 DPVector4;

typedef GLKMatrix2 DPMatrix2;
typedef GLKMatrix3 DPMatrix3;
typedef GLKMatrix4 DPMatrix4;


@interface NSString(DPCube)
-(bool) isNumeric;
@end

@implementation NSString(DPCube)

-(bool) isNumeric{
    
    static NSNumberFormatter* numberFormatter = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        numberFormatter = [[NSNumberFormatter alloc] init];
        //Set the locale to US
        [numberFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
        //Set the number style to Scientific
        [numberFormatter setNumberStyle:NSNumberFormatterScientificStyle];
    });
    
    if ([numberFormatter numberFromString:self] != nil)
        return YES;
    
    return NO;
}
@end

@interface DPCubeLUTFileProvider()
@property (nonatomic,strong) NSString   *title;
@property (nonatomic,assign) DPVector3  domainMin;
@property (nonatomic,assign) DPVector3  domainMax;
@property (nonatomic,assign) NSUInteger lut3DSize;
@property (nonatomic,readonly) DPCLUTType type;
@end

@implementation DPCubeLUTFileProvider {
    NSData* dataout;
}

- (id)init {
    self = [super init]; 
    if (self){}
    return self;
}

- (id)initWithContents: (NSString *)contents {
    self = [super init];
    if (self != NULL) {
        dataout = [self updateLUTFile:contents];
    }
    return self;
}

- (NSData *) updateLUTFile:(NSString *)contents {
    @autoreleasepool {
        BOOL useFloatLUT = NO; // [[[UIDevice currentDevice] systemVersion] floatValue]>=9.0;
        
        _domainMin.r = 0.0;
        _domainMin.g = 0.0;
        _domainMin.b = 0.0;
        _domainMax.r = 1.0;
        _domainMax.g = 1.0;
        _domainMax.b = 1.0;
        
//        NSString *cubeFile = [[NSBundle mainBundle] pathForResource:name ofType:@"cube"];
//        
//        if (cubeFile==nil) {
//            return nil;
//        }
//        
//        NSString *contents = [NSString stringWithContentsOfFile:cubeFile encoding:NSUTF8StringEncoding error:nil];
        
        NSArray *lines = [contents componentsSeparatedByCharactersInSet: [NSCharacterSet newlineCharacterSet]];
        
        NSInteger  linenum=0;
        //NSMutableArray    *data = [NSMutableArray new];
        
        NSMutableData *dataBytes = [[NSMutableData alloc] init];
        
        BOOL isData=NO;
        
        
        for (NSString *line in lines) {
            linenum++;
            NSArray *words =
            [line componentsSeparatedByCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
            
            if ([line hasPrefix:@"#"] || [words[0] length]==0) {
                continue;
            }
            else{
                
                NSString *keyword = words[0];
                
                if (words.count>1) {
                    if ([[keyword uppercaseString] hasPrefix:@"TITLE"]) {
                        _title = words[1];
                    }
                    else if ([[keyword uppercaseString] hasPrefix:@"DOMAIN_MIN"]) {
                        if (words.count==4) {
                            _domainMin.r=[words[1] floatValue];
                            _domainMin.g=[words[2] floatValue];
                            _domainMin.b=[words[3] floatValue];
                        }
                        else{
                            return nil;
                        }
                    }
                    else if ([[keyword uppercaseString] hasPrefix:@"DOMAIN_MAX"]) {
                        if (words.count==4) {
                            _domainMax.r=[words[1] floatValue];
                            _domainMax.g=[words[2] floatValue];
                            _domainMax.b=[words[3] floatValue];
                        }
                        else{
                            return nil;
                        }
                    }
                    else if ([[keyword uppercaseString] hasPrefix:@"LUT_3D_SIZE"]) {
                        _lut3DSize = [words[1] floatValue];
                        _type = DP_CUBE_LUT_TYPE_3D;
                        if ( _lut3DSize < 2 || _lut3DSize > 256 ) {
                            return nil;
                        }
                    }
                    else if ([[keyword uppercaseString] hasPrefix:@"LUT_1D_SIZE"]) {
                        _lut3DSize = [words[1] floatValue];
                        _type = DP_CLUT_TYPE_1D;
                        if ( _lut3DSize < 2 || _lut3DSize > 65536 ) {
                            return nil;
                        }
                    }
                    else if ([[keyword uppercaseString] hasPrefix:@"LUT_1D_INPUT_RANGE"]) {
                        if (words.count==3) {
                            
                            float dmin = [words[1] floatValue];
                            float dmax = [words[2] floatValue];
                            
                            _domainMin.r=_domainMin.g=_domainMin.b=dmin;
                            _domainMax.r=_domainMax.g=_domainMax.b=dmax;
                        }
                        else{
                            return nil;
                        }
                    }
                    else if (isData || [keyword isNumeric]){
                        if (
                            (_domainMax.r-_domainMin.r)<=0
                            ||
                            (_domainMax.g-_domainMin.g)<=0
                            ||
                            (_domainMax.b-_domainMin.b)<=0
                            ) {
                            return nil;
                        }
                        
                        isData = YES;
                        
                        float denom = !useFloatLUT?255.0f:1.0;
                        
                        Float32 r = [words[0] floatValue]/(_domainMax.r-_domainMin.r)*denom;
                        Float32 g = [words[1] floatValue]/(_domainMax.g-_domainMin.g)*denom;
                        Float32 b = [words[2] floatValue]/(_domainMax.g-_domainMin.g)*denom;
                        Float32 a = denom;
                        
                        if (!useFloatLUT) {
                            uint8_t ri = (uint8_t)r;
                            uint8_t gi = (uint8_t)g;
                            uint8_t bi = (uint8_t)b;
                            uint8_t ai = (uint8_t)a;
                            [dataBytes appendBytes:&ri length:sizeof(uint8_t)];
                            [dataBytes appendBytes:&gi length:sizeof(uint8_t)];
                            [dataBytes appendBytes:&bi length:sizeof(uint8_t)];
                            [dataBytes appendBytes:&ai length:sizeof(uint8_t)];
                        }
                        else{
                            [dataBytes appendBytes:&r length:sizeof(Float32)];
                            [dataBytes appendBytes:&g length:sizeof(Float32)];
                            [dataBytes appendBytes:&b length:sizeof(Float32)];
                            [dataBytes appendBytes:&a length:sizeof(Float32)];
                        }
                    }
                }
                else{
                    return nil;
                }
            }
        }
        
        return dataBytes;
    }
}

- (NSData *) getData {
    return dataout;
}

- (NSUInteger) size {
    return _lut3DSize;
}

@end
