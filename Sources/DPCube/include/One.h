#import <Foundation/Foundation.h>
@interface DPCubeLUTFileProvider : NSObject
- (id)init;
- (id)initWithContents: (NSString *)contents;
- (NSData *) getData;
- (NSUInteger) size;
@end
