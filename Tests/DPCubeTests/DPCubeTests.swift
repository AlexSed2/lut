import XCTest
@testable import DPCube

final class DPCubeTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(DPCube().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
