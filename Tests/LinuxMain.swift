import XCTest

import DPCubeTests

var tests = [XCTestCaseEntry]()
tests += DPCubeTests.allTests()
XCTMain(tests)
